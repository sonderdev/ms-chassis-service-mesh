# Microservices - Service Mesh
## :triangular_ruler: Architecture
The Service Mesh provides a centralised mechanism for implementing microservice patterns such as monitoring, logging, circuit breaking, etc, which allows the individual microservices to be as simple as possible.

It also provides a central routing mechanism for requests, if Service A wants to make a request to Service B, then it does so through the mesh. Service A does not need to know the location of Service B, just that it would like to contact it, the Service Mesh forwards the request to the right location using the data held in the Service Registry.

![Architecture](img/architecture.png)

## :twisted_rightwards_arrows: Routing
The Service Mesh is a heavily used service, no microservices should be contacted via http calls unless the request comes from the Service Mesh, therefore it needs to be highly available, capable of handling high load and scaling horizontally. Even requests coming from the internet via the API Gateway will need to go through the Service Mesh.

## :white_check_mark: Required Headers
A prequisite to sending a request is to have certain headers attached to allow the Service Mesh to log effectively, these are:

* ***x-sonder-source*** - the source service of the request (**MANDATORY**)
* ***x-sonder-request-id*** - the original request ID which triggered this new request (if not provided, the Service Mesh will create on and attach it to the request which goes to the destination service)
* ***x-sonder-user-id*** - the ID of the user for which the request is being performed (if not provided and a JWT is found then the Service Mesh will add the user id to the outbound request)

## :no_entry_sign: Circuit breaking
If a single instance of the Service Mesh receives a certain number of `5xx` errors from forwarding http requests to a given microservice in a certain amount of time then it will `open` the circuit breaker for that microservice by posting an update to the Service Registry. This will then prevent requests from being forwarded to the microservice for a certain amount of time when the Service Registry will set the circuit breaker to `half_open`, if any `5xx` errors are recieved when a circuit breaker is `half_open` then the Service Mesh will `open` the breaker again. If the breaker stays `half_open` for a certain amount of time the Service Registry will `close` the breaker.

## :pencil: Logging
For every request made the Service Mesh will log the request to CloudWatch, instantly giving a detailed view of all of the requests going around the system. This can help to provide monitoring and alarms for all microservices out of the box.

## :cop: Authentication
To start, the Service Mesh will not perform any authentication as no request should make this far without already being authenticated.