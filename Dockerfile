FROM golang:1.10.3 AS build-env
WORKDIR /go/src/bitbucket.org/sonderdev/ms-chassis-service-mesh/src
ADD src . 
RUN go get github.com/gorilla/mux
RUN go get github.com/joho/godotenv
RUN go get github.com/satori/go.uuid
RUN go get github.com/aws/aws-sdk-go/aws
RUN go get gopkg.in/olivere/elastic.v5
RUN go test -v ./tests	
RUN CGO_ENABLED=0 GOOS=linux go build -o main.exe main.go

FROM scratch
COPY --from=build-env /go/src/bitbucket.org/sonderdev/ms-chassis-service-mesh/src/main.exe .
COPY --from=build-env /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
ENV PORT=80
ENV SERVICE_REGISTRY_URL=http://service-registry/
ENTRYPOINT  ["./main.exe"]
EXPOSE 80