package main

import (
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"

	"bitbucket.org/sonderdev/ms-chassis-service-mesh/src/factories"
	"bitbucket.org/sonderdev/ms-chassis-service-mesh/src/logging"
	"bitbucket.org/sonderdev/ms-chassis-service-mesh/src/relay"
	"bitbucket.org/sonderdev/ms-chassis-service-mesh/src/serviceRepo"
	"bitbucket.org/sonderdev/ms-chassis-service-mesh/src/transport"
)

var (
	r  relay.Relay
	t  transport.Transport
	sr = serviceRepo.ServiceRepo{}
)

func main() {
	// load env variables
	err := godotenv.Load()
	if err != nil {
		log.Print("Error loading .env file")
	}

	l := logging.NewLogger(factories.NewLoggingClient())

	r = relay.New(l)

	router := mux.NewRouter()

	t = transport.New(10, 120)

	router.PathPrefix("/{service}/").HandlerFunc(requestHandler)

	// get the port
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	srv := &http.Server{
		Handler:      router,
		Addr:         os.Getenv("HOST") + ":" + port,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	sr.StartRefeshing(&t)

	log.Fatal(srv.ListenAndServe())
}

func requestHandler(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)

	s, err := sr.GetService(vars["service"])

	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		io.Copy(w, strings.NewReader("404 not found"))
		return
	}

	r.RelayRequest(s, t, w, req)
}
