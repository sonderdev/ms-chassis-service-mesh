package transport

import (
	"bytes"
	"fmt"
	"net/http"
	"time"

	uuid "github.com/satori/go.uuid"
)

// Transport ...used to make http requests
type Transport struct {
	client *http.Client
}

// New ...Transport constructor
func New(MaxIdleConns int, IdleConnTimeout int) Transport {
	transport := Transport{}
	tr := &http.Transport{
		MaxIdleConns:       MaxIdleConns,
		IdleConnTimeout:    time.Duration(IdleConnTimeout) * time.Second,
		DisableCompression: true,
	}
	transport.client = &http.Client{Transport: tr}

	return transport
}

// MakeRequest ...makes the actual request
func (transport Transport) MakeRequest(r Request) (resp *http.Response, request_id string, err error) {
	req, err := http.NewRequest(r.Method, r.URL, bytes.NewReader(r.Body))
	if err != nil {
		return nil, "", err
	}

	// map the headers
	req.Header = r.Header

	// if there weren't any, make some new ones
	if req.Header == nil {
		req.Header = make(http.Header)
	}

	// make a request_id and add it to the request
	u, err := uuid.NewV4()
	if err != nil {
		return nil, fmt.Sprint(u), err
	}
	req.Header.Add("request_id", fmt.Sprint(u))

	// make the request
	resp, err = transport.client.Do(req)

	// return everything
	return resp, fmt.Sprint(u), err
}

// Request ...a transport request object
type Request struct {
	URL    string
	Method string
	Header http.Header
	Body   []byte
}
