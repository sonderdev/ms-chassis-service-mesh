package serviceRepo

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"time"

	"bitbucket.org/sonderdev/ms-chassis-service-mesh/src/dto"
	"bitbucket.org/sonderdev/ms-chassis-service-mesh/src/transport"
)

type ServiceRepo struct {
	services []dto.Service
}

func (sr ServiceRepo) GetService(name string) (dto.Service, error) {
	for _, s := range sr.services {
		if s.Name == name {
			return s, nil
		}
	}

	return dto.Service{}, errors.New("Could not find service")
}

func (sr *ServiceRepo) refreshServices(t *transport.Transport) {
	tr := transport.Request{}
	tr.URL = os.Getenv("SERVICE_REGISTRY_URL")
	tr.Method = "GET"

	resp, _, err := t.MakeRequest(tr)

	if err != nil {
		fmt.Printf("Services failed to refresh\n")
		return
	}

	_ = json.NewDecoder(resp.Body).Decode(&sr.services)
}

func (sr *ServiceRepo) StartRefeshing(t *transport.Transport) {
	sr.refreshServices(t)
	ticker := time.NewTicker(5 * time.Second)
	quit := make(chan struct{})
	go func() {
		for {
			select {
			case <-ticker.C:
				sr.refreshServices(t)
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
}
