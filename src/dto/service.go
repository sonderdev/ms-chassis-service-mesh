package dto

type Service struct {
	Name           string
	Location       string
	Status         string
	CircuitBreaker string
	Retries        int
}
