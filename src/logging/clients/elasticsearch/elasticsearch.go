package elasticsearch

import (
	"context"
	"os"
	"time"

	"bitbucket.org/sonderdev/ms-chassis-service-mesh/src/logging"
	elastic "gopkg.in/olivere/elastic.v5"
)

type elasticsearchLogger struct {
	client *elastic.Client
}

func New() *elasticsearchLogger {
	elasticURL := os.Getenv("LOGGING_ELASTIC_URL")
	client, err := elastic.NewSimpleClient(elastic.SetURL(elasticURL))
	if err != nil {
		panic(err)
	}

	esl := elasticsearchLogger{}
	esl.client = client

	return &esl
}

func (esl *elasticsearchLogger) LogRequest(l *logging.RequestLog) {
	index := "service-mesh-" + time.Now().Format("2006-01-02")
	ctx := context.Background()
	esl.client.Index().Index(index).Type("request").Id(l.RequestID).BodyJson(&l).Do(ctx)
}
