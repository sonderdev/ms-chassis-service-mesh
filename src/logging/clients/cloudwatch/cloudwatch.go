package cloudwatch

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sync"
	"time"

	"bitbucket.org/sonderdev/ms-chassis-service-mesh/src/logging"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudwatchlogs"
)

var (
	LOGGROUPNAME  string
	LOGSTREAMNAME string
)

type cloudwatchLogger struct {
	svc           *cloudwatchlogs.CloudWatchLogs
	bufferedLogs  []*cloudwatchlogs.InputLogEvent
	sequenceToken *string
	lock          bool
	m             sync.Mutex
}

func New() *cloudwatchLogger {
	LOGGROUPNAME = os.Getenv("AWS_CLOUDWATCH_LOGGROUP_NAME")
	LOGSTREAMNAME = os.Getenv("HOSTNAME")

	cwl := cloudwatchLogger{}
	sess := session.Must(session.NewSession())
	cwl.svc = cloudwatchlogs.New(sess, aws.NewConfig().WithRegion(os.Getenv("AWS_REGION")))
	cwl.lock = false

	// The first thing to do is create a log stream, the group should have been created by CloudFormation
	// If this fails then panic, we MUST log
	cwl.createLogStream()

	// get the sequence token
	dlsi := cloudwatchlogs.DescribeLogStreamsInput{}
	dlsi.LogGroupName = &LOGGROUPNAME
	dlsi.LogStreamNamePrefix = &LOGSTREAMNAME
	ls, err := cwl.svc.DescribeLogStreams(&dlsi)
	if err != nil {
		log.Panic("Could not get sequence token: ", err)
	}

	cwl.sequenceToken = ls.LogStreams[0].UploadSequenceToken

	// iterate over bufferedLogs and periodically send them
	go cwl.startTimer()

	return &cwl
}

func (cwl *cloudwatchLogger) LogRequest(l *logging.RequestLog) {
	cwl.m.Lock()
	defer cwl.m.Unlock()

	lg, err := json.Marshal(l)
	if err != nil {
		fmt.Println(err)
		return
	}

	logstr := string(lg)
	ts := l.EndTime.UnixNano() / int64(time.Millisecond)

	ile := cloudwatchlogs.InputLogEvent{}
	ile.Message = &logstr
	ile.Timestamp = &ts

	for cwl.lock == true {
		// pause here if locked
	}
	log.Println("Appending log")
	cwl.bufferedLogs = append(cwl.bufferedLogs, &ile)

}

func (cwl *cloudwatchLogger) startTimer() {
	for {
		time.Sleep(time.Second * 5)
		log.Println(cwl.bufferedLogs)
		bl := cwl.bufferedLogs
		if len(bl) > 0 {
			cwl.sendLogs()
		}
	}
}

func (cwl *cloudwatchLogger) sendLogs() error {
	cwl.m.Lock()
	defer cwl.m.Unlock()

	log.Println("Sending logs")
	ple := cloudwatchlogs.PutLogEventsInput{}

	ple.LogGroupName = &LOGGROUPNAME
	ple.LogStreamName = &LOGSTREAMNAME
	ple.SequenceToken = cwl.sequenceToken
	ple.LogEvents = cwl.bufferedLogs

	res, err := cwl.svc.PutLogEvents(&ple)
	if err == nil {
		cwl.bufferedLogs = []*cloudwatchlogs.InputLogEvent{}
		cwl.sequenceToken = res.NextSequenceToken
	} else {
		log.Println(err)
	}
	cwl.lock = false
	return err
}

func (cwl *cloudwatchLogger) createLogStream() {

	lsi := cloudwatchlogs.CreateLogStreamInput{}
	lsi.LogGroupName = &LOGGROUPNAME
	lsi.LogStreamName = &LOGSTREAMNAME
	_, err := cwl.svc.CreateLogStream(&lsi)
	if err != nil {
		log.Println(err)
	}

}
