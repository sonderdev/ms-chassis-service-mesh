package stdout

import (
	"fmt"

	"bitbucket.org/sonderdev/ms-chassis-service-mesh/src/logging"
)

type stdOutLogger struct {
}

func New() *stdOutLogger {
	sol := stdOutLogger{}
	return &sol
}

func (std stdOutLogger) LogRequest(l *logging.RequestLog) {
	fmt.Printf("%+v\n", *l)
}
