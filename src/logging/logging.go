package logging

import (
	"time"

	"bitbucket.org/sonderdev/ms-chassis-service-mesh/src/dto"
)

type RequestLog struct {
	RequestID             string
	ParentRequestID       string
	ServiceName           string
	ServiceLocation       string
	ServiceStatus         string
	ServiceRetryLimit     int
	ServiceCircuitBreaker string
	URI                   string
	Method                string
	StatusCode            int
	StartTime             time.Time
	EndTime               time.Time
	Duration              float64
	Attempt               int
	ErrorDetails          string
}

type LoggingClient interface {
	LogRequest(*RequestLog)
}

type Logger struct {
	LoggingClient LoggingClient
}

func NewLogger(lc LoggingClient) *Logger {
	return &Logger{
		lc,
	}
}

func (l Logger) NewRequestLog(s dto.Service) RequestLog {
	rl := RequestLog{}
	rl.StartTime = time.Now()
	rl.ServiceName = s.Name
	rl.ServiceLocation = s.Location
	rl.ServiceStatus = s.Status
	rl.ServiceRetryLimit = s.Retries
	rl.ServiceCircuitBreaker = s.CircuitBreaker

	return rl
}

func (l Logger) EndRequest(rl RequestLog, requestID string, statusCode int, err string) {
	rl.EndTime = time.Now()
	rl.RequestID = requestID
	rl.StatusCode = statusCode
	rl.ErrorDetails = err
	rl.Duration = float64(rl.EndTime.Sub(rl.StartTime)) / float64(time.Millisecond)

	l.LoggingClient.LogRequest(&rl)
}
