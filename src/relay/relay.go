package relay

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"bitbucket.org/sonderdev/ms-chassis-service-mesh/src/dto"
	"bitbucket.org/sonderdev/ms-chassis-service-mesh/src/logging"
	"bitbucket.org/sonderdev/ms-chassis-service-mesh/src/transport"
)

type Relay struct {
	logger *logging.Logger
}

func New(l *logging.Logger) Relay {
	return Relay{
		l,
	}
}

func (relay Relay) RelayRequest(s dto.Service, t transport.Transport, w http.ResponseWriter, r *http.Request) {
	redirect := getUrl(s, r.URL.Path)

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		io.Copy(w, strings.NewReader("404 page not found"))
		return
	}

	req := transport.Request{
		redirect,
		r.Method,
		r.Header,
		body,
	}

	// right here we need to invoke logging, first to build logging object with the start of the request
	rl := relay.logger.NewRequestLog(s)
	rl.Method = r.Method
	rl.URI = strings.Replace(redirect, s.Location, "/", 1)
	rl.ParentRequestID = r.Header.Get("request_id")

	// don't do anything if the service isn't alive and the circuit breaker is open
	if s.Status != "alive" || s.CircuitBreaker == "open" {
		w.WriteHeader(http.StatusServiceUnavailable)
		io.Copy(w, strings.NewReader("503 service not available"))
		go relay.logger.EndRequest(rl, "", http.StatusServiceUnavailable, "503 service not available")
		return
	}

	var resp *http.Response
	var requestID string

	tries := 0
	for tries <= s.Retries {
		resp, requestID, err = t.MakeRequest(req)
		rl.Attempt = tries + 1

		if err != nil || resp.StatusCode >= 500 {
			tries = tries + 1
			// finish off the logging object and send it to the logger via a goroutine, including error details
			go relay.logger.EndRequest(rl, requestID, 0, err.Error())
		} else {

			// finish off the logging object and send it to the logger via a goroutine
			go relay.logger.EndRequest(rl, requestID, resp.StatusCode, "")

			tries = s.Retries + 1
			defer resp.Body.Close()
			copyHeader(w.Header(), resp.Header)
			w.WriteHeader(resp.StatusCode)
			io.Copy(w, resp.Body)
			return
		}
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusServiceUnavailable)
	fmt.Fprintf(w, "{ \"error\":\"Didn't work\" }")
}

func getUrl(s dto.Service, u string) string {
	return strings.Replace(u, "/"+s.Name+"/", s.Location, 1)
}

func copyHeader(dst, src http.Header) {
	for k, vv := range src {
		for _, v := range vv {
			dst.Add(k, v)
		}
	}
}
