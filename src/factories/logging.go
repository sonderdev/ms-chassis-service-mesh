package factories

import (
	"log"
	"os"

	"bitbucket.org/sonderdev/ms-chassis-service-mesh/src/logging"
	"bitbucket.org/sonderdev/ms-chassis-service-mesh/src/logging/clients/cloudwatch"
	"bitbucket.org/sonderdev/ms-chassis-service-mesh/src/logging/clients/elasticsearch"
	"bitbucket.org/sonderdev/ms-chassis-service-mesh/src/logging/clients/stdout"
)

func NewLoggingClient() logging.LoggingClient {
	lgc := os.Getenv("LOGGING_CLIENT")

	switch lgc {
	case "elasticsearch":
		log.Println("[x] Using ElasticSearch for logging")
		return elasticsearch.New()
	case "cloudwatch":
		log.Println("[x] Using CloudWatch for logging")
		return cloudwatch.New()
	default:
		log.Println("[x] Using stdout for logging")
		return stdout.New()
	}
}
